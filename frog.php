<?php
class Frog extends Animal
{
    public $legs = 4;

    public function __construct($string)
    {
        echo $string . "<br>";
    }
    public function jump()
    {
        echo "hop hop<br>";
    }
}
