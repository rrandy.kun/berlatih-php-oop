<?php
class Animal
{
    public $legs = 2;
    public $cold_blooded = "false";
    public $name;

    public function __construct($string)
    {
        $this->name = $string;
    }
    public function get_cold_blooded()
    {
        return $this->cold_blooded;
    }
}
