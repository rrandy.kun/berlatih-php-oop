<?php
require "animal.php";
require "ape.php";
require "frog.php";

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->get_cold_blooded(); // false
echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell();

$kodok = new Frog("buduk");
$kodok->jump();
